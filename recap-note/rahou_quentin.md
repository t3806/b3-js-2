# RECAP

## 1

Les TP ne sont pas tous rendus : 10 pts

Décompte des 8 points restants :

- Indentation et certains espaces pas respectés : -1
- Nom de variable fr/en : -2
- Pas d'interpolation es6 : -1
- Pas le bon format d'envoi des TP: -1

Total: 13

## 2

- Du css statique (quand on veut avoir plus de cases, ca casse le css): -1
- Camel case motifimpaire -> motifImpaire : -1
- Variable fr/en : -1
- Mauvaise indentation css : -1
- Malus, il y a pas de tentative sur une autre solution d'algo : -5
- Malus, même code que Marie et Daphne (il y a les mêmes espaces avant les divs) : -3

Total: 8

## 3

fetchListUsers: Oui 4/4
fetchOneUser: Non j'attendais un find (regarde la correction) 2/4
updateOneUser: Oui 2/2
postOneUser: Oui 2/2
deleteOneUser: Oui 2/2
postOneUserScore: Pas fait
fetchScoresPodium: Pas fait
fetchListScores: Pas fait

total: 12 - 2( Exactement comme Marie et Daphne) = 10
