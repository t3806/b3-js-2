# RECAP

## 1

Les bases sont validées : 20

## 2

- Tu as une variable en dehors de ta boucle (let chessBoardSize) : -1
- Comment faire pour avoir un plateau de 10 x 10 ? : -1

Total : 18

## 3

fetchListUsers: Oui 4/4
fetchOneUser: Oui 4/4
updateOneUser: Super ! j'avais pas forcément demandé de la gestion d'erreurs mais du coup c'est cool ! on peut optimiser mais très bien déjà ! 2/2
postOneUser: Oui 2/2
deleteOneUser: Oui 2/2
postOneUserScore: Ah mince tu as pas eu le temps de le faire ? 0/2
fetchScoresPodium: Pas fait
fetchListScores: Pas fait

total: 14 + 4 (pour ton travail sur nextJs et on implication !) + 1 pour le kahoot = 18
Par contre tu as push les nodes modules!
