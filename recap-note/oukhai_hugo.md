# RECAP

## 1

Les TP sont rendus mais pas tous les algos, ton TP3 n'est pas bon: 9 pts

Décompte des 8 points restants :

- Indentation et certains espaces pas respectés : -1
- Pas d'arrow function : -1
- Nom de variable fr/en : -2
- Pas d'interpolation es6 : -1

Total: 12

## 2

- Mauvaise indentation HTML : -1
- Mauvaise indentation CSS : -1
- Mauvaise indentation JS : -1
- Une erreur dans le CSS (grid-column-width:200px; unknown property) : -1
- Le damier ne fonctionne pas : -10

Total: 6

## 3

Rien rendu 0
