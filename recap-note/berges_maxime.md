# RECAP

## 1

Les TP ne sont pas tous rendus, le 4 et le 5 n'est pas bon par exemple : 9 pts

Décompte des 8 points restants :

- Utilisation de var : -2
- Indentation et certains espaces pas respectés : -1
- Pas d'arrow function : -1
- Nom de variable fr/en : -2
- Pas d'interpolation es6 : -1
- Mauvaise utilisation ou pas de return: -1

Total: 9

## 2

- Le fichier DOM est pas dans ton dossier : -1
- La nomenclature, on met pas de majuscule au nom de fichier "Damier.html" : -1
- Dans le JS les commentaires ont juste besoin de // au début et pas à la fin : - 1
- Tu as une variable en dehors de ta boucle (let one) : -1
- Tu as 2 boucles : -2
- Les valeurs dans ta boucle sont pas assez logiques pour l'utilisateur ((550 pour 8 ?) : -2
- Système de flag (one = !one) pas très opti mais je te le compte pas, on verra ca en cours

Total: 12

## 3

fetchListUsers: Oui 4/4
fetchOneUser: Oui 4/4
updateOneUser: Oui 2/2 (j'aime beaucoup)
postOneUser: Oui, mais il faut pas mettre l'id en dur 5, il faut que le prochain soit 6 donc dynamique, sinon très propre 2/2
deleteOneUser: Oui, très propre 2/2
postOneUserScore: Oui 2/2
fetchScoresPodium: Pas fait
fetchListScores: Pas fait

total: 16 - 1 pour l'id = 15
Je sais pas si copilot ou quelqu'un qui t'as aidé mais c'est très propre !
