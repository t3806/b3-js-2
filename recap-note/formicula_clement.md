# RECAP

## 1

Les TP sont rendus + les algos: 12 pts

Décompte des 8 points restants :

- Indentation et certains espaces pas respectés : -1
- Les TP ne sont pas rendus dans le format demandé : -2
- Copié collé avec ton pote Alexandre sur le TP 5 : -3
- Pas d'interpolation es6 : -1

Total: 13

## 2

- L'indentation : -2
- Tu as 2 boucles : -2
- Malus, tu as donné ton code en retard : -5
- La copie du code de Valentin : -5

Total: 6

## 3

fetchListUsers: Oui 2/4 Tu as pas utilisé promise simulator
fetchOneUser: Non 0/4 ton code fonctionne pas
updateOneUser: Non 0/2 pareil il a des erreurs partout
postOneUser: Non 0/2 pareil
deleteOneUser: Moité 1/2
postOneUserScore: Non 0/2 pareil
fetchScoresPodium: Pas fait
fetchListScores: Pas fait

total: 3 Je peux même pas lancer le serveur avec tes erreurs !
