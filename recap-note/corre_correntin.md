# RECAP

## 1

Les TP sont rendus + les algos: 12 pts

Décompte des 8 points restants :

- Indentation et certains espaces pas respectés : -1
- Nom de variable fr/en : -2

kahoot: 0,5

Total: 17,5

## 2

- Tu as une variable en dehors de ta boucle (let one) : -1
- Je te le compte pas mais pour un commentaire sur une seule ligne on utilise "//"

Très bon travail

Total : 19

## 3

fetchListUsers: Oui 4/4
fetchOneUser: Oui 4/4
updateOneUser: Moité (tu as modifié "updatedUser" mais pas celui dans la database, regarde la correction 1/2
postOneUser: Oui 2/2
deleteOneUser: Oui 2/2
postOneUserScore: Non 0/2 Il y a un soucis avec ton newOne et en plus tu affectes pas la data dans la database (regarde la correction)
fetchScoresPodium: Pas fait
fetchListScores: Pas fait

total: 13 + 1 pour le kahoot = 14
Tu as push les nodes modules!
