# RECAP

## 1

Les TP sont rendus + les algos: 12 pts

Décompte des 8 points restants :

- Indentation et certains espaces pas respectés : -1
- Pas d'arrow function : -1
- Nom de variable fr/en : -2
- Pas d'interpolation es6 : -1
- Mauvase gestion des return ou pas de return : -1

Total: 14

## 2

- Indentation dans l'HTML : -1
- Double boucle dans le JS : -2
- Le nom de la const ?? colorNormal ? c'est pas ce que la fonction fait : -1

Sinon très bien, fonction fléchée et bonne utilisation du modulo, bravo !

TOTAL: 16

## 3

fetchListUsers: Oui 4/4
fetchOneUser: Oui 4/4
updateOneUser: Moité (tu as modifié "userUpdated" mais pas celui dans la database, regarde la correction 1/2
postOneUser: Oui, mais il faut mettre l'id dans le nouvel utilisateurs 2/2
deleteOneUser: Motié, tu as la logique mais il faut utiliser splice et pas slice pouvoir faire le changement dans la database 1/2
postOneUserScore: Moitié, pareil que postOneUser, il faut update l'id 2/2
fetchScoresPodium: Pas fait
fetchListScores: Pas fait

total: 14 - 1 pour l'id = 13
