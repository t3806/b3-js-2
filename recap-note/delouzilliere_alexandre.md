# RECAP

## 1

Les TP sont rendus + les algos: 12 pts

Décompte des 8 points restants :

- Indentation et certains espaces pas respectés : -1
- Les TP ne sont pas rendus dans ton dossier : -2
- Copié collé avec ton pote Clement sur le TP 5 : -3
- Pas d'interpolation es6 : -1

Total: 13

## 2

- Nom du dossier en francais avec un accent alors que les fichiers à l'intérieur sont en anglais : -1
- Mauvaise indentation du CSS : -1
- Des variables sont en PascalCase (ChessTable ? ca doit être chessTable) : -2
- Mauvaise indentation du JS ( Très dur à lire ) : -1
- Pas de facilitateur pour avoir un damier à 10 cases, et si je le fait à la mano le css bug : -1
- Des attributs css fr/en (caseblanche, center) : -2
- Une double boucle : -2
- Utilisation de var : -3

Total: 7

## 3

fetchListUsers: Oui 2/4 Tu as pas utilisé promise simulator
fetchOneUser: Oui 2/4 Idem
updateOneUser: Moité (tu as modifié "updatedOneUser" mais pas celui dans la database, regarde la correction 1/2
postOneUser: Oui 2/2
deleteOneUser: Oui 2/2
postOneUserScore: Moitié 1/2 Tu affectes pas la data dans la database (regarde la correction)
fetchScoresPodium: Pas fait
fetchListScores: Pas fait

total: 10
Tu as push les nodes modules!
