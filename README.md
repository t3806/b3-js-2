# B3-JS-2

## Présentation du Cours

Ce cours sera divisé en 3 modules : les bases de Javascript, Le DOM/promises et enfin l'utilisation de Javascript dans un cas pratique avec un framework.

Prérequis :

Savoir utiliser git, connaître son environnement de dev et avoir quelques notions en algo.

### Travail personnel

- Ayez un compte gitlab (de préférence à votre nom)
- Clonez ce dépot git
- Créez une branche préfixé STU- à votre nom : (exemple STU-touron-gabriel pour gabriel touron) et dans cette branche un dossier à votre nom-prenom dans le dossier students.
  Dans ce dossier un fichier me.txt de la forme :

```txt
student : Nom Prenom
email : votre adresse mail
presentation :
  je me présente en 3 lignes et dire en quoi le JS me serait utile.
evaluation du cour :
  toutes vos remarques sur le cours sont ici les bienvenues,
```

- Vous placerez dans ce dossier vos travaux
- Vous pousserez votre branche sur le dépot git du cours régulièrement

Il faudra rebase main pour avoir la correction une fois le repo mis à jour `git rebase origin/main`.

### Contributions

- Revenez sur la branche main du dépot `git checkout main ; git pull`
- Créez une nouvelle branche FIX-le-sujet-du-fix `git checkout -b FIX-le-sujet-du-fix`
- Corrigez , ajoutez , commitez `man git`
- Poussez votre branche `git push --set-upstream`
- Faites moi une merge request (cliquez sur le lien que vous retourne gitlab)
- Retourner sur votre branche de travail

Sytème de cours récupéré d'Alan Simon (Une personne très compétente)

D'avance merci

## 1 - [Présentation](https://courses.codebuds.com/course/JavaScript#/introduction) & Fondations

Suivre le lien de notre site codebuds courses : [introduction](https://courses.codebuds.com/course/JavaScript#/introduction)

Installation :

- Node : Suivre les instructions de [node.js](https://nodejs.org/en/)
- Npm : Suivre les instructions de [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

Reprendre ses bases :

```bash
npm install -g javascripting
```

Créer un dossier base dans votre répertoire `mkdir base`

TP : Faire les TP du [cours](https://courses.codebuds.com/course/JavaScript#/tp1), vous allez pouvoir mettre vos scripts dans le dossier `base` dans un sous dossier nommé par le numéro du TP `cd base ; mkdir TP-1` (pour le premier TP)

Ressources :

- [Fireship](https://www.youtube.com/channel/UCsBjURrPoezykLs9EqgamOA)
- [30-days-of-javascript](https://github.com/Asabeneh/30-Days-Of-JavaScript) (Base jusqu'au 11)

Suivi du cours :

- [x] day-1
- [x] day-2
- [x] day-3

## 2 - Dom & Promise

Voir les chapitres correspondants [30-days-of-javascript](https://github.com/Asabeneh/30-Days-Of-JavaScript)

TP DOM :

- S'inscrire sur le site [Codepen](https://codepen.io/)
- Faire un plateau d'echec en HTML/CSS/JS. C'est le JS qui doit pouvoir le générer dans une seule `div`

![image exemple](./files/DOM/chest-board.png)

- BONUS : en une seule variable on peut transformer le jeu d'echec en jeu de dame (10 cases)

Suivi du cours :

- [x] day-4
- [x] day-5
- [x] day-6

- CORRECTION : [Correction-DOM](./correction/DOM)

## 3 - Data & Projet full-stack

Ce dernier module vise à reproduire une situation courante en Javascript, l'exploitation de la data.
Les connaissances a avoir pour faire ce projet final :

- Les promises : [30-days-of-javascript-promises](https://github.com/Asabeneh/30-Days-Of-JavaScript/tree/master/18_Day_Promises)
- JSON : [30-days-of-javascript-promises](https://github.com/Asabeneh/30-Days-Of-JavaScript/blob/master/16_Day_JSON/16_day_json.md)
- Serveur : todo

TP Kahoot :

> Formulaire pour poster des users, des scores (et des parties).
> Affichage de la moyenne des score, et le podium

```txt
Routes :

USER
POST/ users
DELETE/ users/id ou ids
GET/ users
GET/ user/id

SCORES

GET/ scores
GET/ scores/id
POST/ scores
DELETE/ scores/id ou ids

(BONUS) PARTY
POST/ parties
DELETE/ parties
GET/ parties
GET/ parties/id
```

- [x] day-7
- [ ] day-8
- [ ] day-9
- [ ] day-10
