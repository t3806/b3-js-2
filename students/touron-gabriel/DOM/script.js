const chest = document.getElementById("chest");
// change base 8 => chest, 10 => dame
const base = 10;
chest.style.gridTemplateColumns = `repeat(${base}, auto)`;
for (let i = 1; i < base ** 2 + 1; i++) {
  let z = Math.floor((base ** 2 - i) / base);
  const square = document.createElement("div");
  (i % 2 && z % 2) || (i % 2 == 0 && z % 2 == 0)
    ? square.classList.add("black")
    : square.classList.add("square");
  document.getElementById("chest").appendChild(square);
}

// OTHER SOLUTION

// const chest = document.getElementById('chest')
// const base = 8
// chest.style.gridTemplateColumns = `repeat(${base}, auto)`
// let a = 0
// let b = base
// for (let i = 0; i < base * base ; i++ ) {
//   const square = document.createElement('div')
//   square.classList.add("square")

//   if (i%2 && i>a && i<b) square.classList.add("black")
//   // Si la ligne commence par un carré blanc

//   if (i%2 == 0 && i>=b && i<b + base) square.classList.add("black")
//   // Si la ligne commence par un carré noir

//   if (i == b + base) {a = b + base; b = a + base; }
//   // Dès que 2 lignes sont créées passer au suivantes...

//   chest.appendChild(square)
// }
